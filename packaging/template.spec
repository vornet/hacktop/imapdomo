%global shellfu_req shellfu >= 0.10.4, shellfu < 0.11

Name:       __MKIT_PROJ_PKGNAME__
Version:    __MKIT_PROJ_VERSION__
Release:    1%{?dist}
Summary:    __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:        __MKIT_PROJ_VCS_BROWSER__
License:    GPLv2
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
BuildRequires: make

Requires: %shellfu_req
Requires: imapfilter
Requires: shellfu-bash-pretty
%description
imapdomo is simple convenience wrapper around imapfilter that
adds few more Lua functions and suggests some system to way how you can
organize your mail filtering and/or migrations, etc.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
%make_install

%files
%dir %{_datadir}/%{name}
%{_bindir}/%{name}
%{_datadir}/%{name}/imapdomo.lua
%{_datadir}/%{name}/imaprules.lua
%{_datadir}/%{name}/main.lua

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
