local pkg = {}

pkg.STATE = {}

pkg.SET = function(loader, ruleset)
    pkg.STATE.loader = loader or function() return true end
    pkg.STATE.ruleset = ruleset or {}
    return true
end

pkg.RUN = function()
    local i
    for _, rule in ipairs(pkg.STATE.ruleset) do
        i = rule.loader()
        if not i then return end
        rule.action(i)
    end
    pkg.SET()
end

pkg.ADD = function(action, loader)
    local rule = {}
    rule.action = action
    rule.loader = loader or pkg.STATE.loader
    table.insert(pkg.STATE.ruleset, rule)
end

return pkg
