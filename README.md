IMAPDOMO
========

imapdomo is simple convenience wrapper around [imapfilter][1] that
adds few more Lua functions and suggests some system to way how you can
organize your mail filtering and/or migrations, etc.

  [1]: https://github.com/lefcha/imapfilter

The basic idea is to provide guidelines as to where to put your Lua rules
separated to several basic 'actions'; and then add a script on top of it
that can be conveniently called inside your crontab.


Development
-----------

As of versions 0.0.Z, almost everything here is subject to breaking
change without warnings or even announcement; let me know if you want
to use this so I should be more careful.

Currently there are no plans to change this "development model".

Note that author of this script is just learning Lua, a bit here and a
bit there, most of stuff is really just hacked up and ugly (especially
to a trained Lua reader).
