#!/bin/bash
# MKit - simple install helper
# See LICENSE file for copyright and license details.

mkit_import build
mkit_import target

rpmstuff__main() {
    #
    # Build specfile
    #
    local specname=$MKIT_PROJ_PKGNAME.spec      # .spec filename
    local specsrc                               # source of specfile
    target__run dist || die
    specsrc="$(ini 1value "dist:rpmstuff")"
    test -n "$specsrc" || die "dist:rpmstuff not specified"
    test -f "$specsrc" || die "specfile template not found: $specsrc"
    __rpmstuff__maybe_prefix_v0 "$specsrc"
    build__file "$specsrc" "$specname" rpmstuff
}

__rpmstuff__maybe_prefix_v0() {
    #
    # Avoid "normalizes to zero" error
    #
    # Scripting on Fedora 38 contains new check, where the version
    # is not allowed to be 0.0.0.
    #
    # We will disable this check for projects where our reported
    # version is indeed 0.0.0, but only if it's also a devel version,
    # in which case it has build data.
    #
    local file=$1
    local tmp="$1.__rpmstuff__maybe_prefix_v0.tmp"
    local version
    version=$(build__cached semver)
    case $version in
        0.0.0+*)
            warn "disabling zero-version rpmbuild check" \
                 " .. hint: We're doing this only because we trust" \
                 " .. hint: that this is an early devel version." \
                 " .. hint: Make sure to bump at least to 0.0.1" \
                 " .. hint: before release."
            ;;
        *)
            return 0
            ;;
    esac
    cp -ar "$file" "$tmp"
    {
        echo "%define _python_dist_allow_version_zero 1"
        echo "# ^^ auto-added by mkit rpmstuff for early devel version"
        echo ""
        cat "$file"
    } > "$tmp"
    mv "$tmp" "$file"
}
