#!/bin/bash
# MKit - simple install helper
# See LICENSE file for copyright and license details.

mkit_import ini
mkit_import plugin

target__ls() {
    #
    # List valid routes
    #
    target__map | cut -d' ' -f1
}

target__map() {
    #
    # List valid routes and corresponding module:functions
    #
    #shellcheck disable=SC2291
    {
        echo _mkit_builddata        build:_mkit_builddata
        echo _mkit_inidata          build:_mkit_inidata
        echo _mkit_metadata         build:_mkit_metadata
        echo _mkit_show_builddata   build:_mkit_show_builddata
        echo _mkit_show_metadata    build:_mkit_show_metadata
        echo build                  build:build
        echo clean                  build:clean
        echo dist                   build:dist
        echo install                deploy:install
        echo release                release:release
        echo release_x              release:release_x
        echo release_y              release:release_y
        echo release_z              release:release_z
        echo uninstall              deploy:uninstall
        echo vbump                  release:vbump
        echo vbump_x                release:vbump_x
        echo vbump_y                release:vbump_y
        echo vbump_z                release:vbump_z
    }
}

target__isvalid() {
    #
    # True if target $1 is valid
    #
    local target=$1
    target__map | grep -qwe "^$target"
}

target__route() {
    #
    # Call correct function based on $1
    #
    local target=$1
    local es
    if target__isvalid "$target"; then
        target__run "$target"; es=$?
    elif plugin__isvalid "$target"; then
        plugin__handle "$target" main; es=$?
    else
        {
            echo "usage: $(basename "$0") TARGET"
            echo
            echo "valid targets (built-in):"
            target__ls | sed 's/^/    /'
            echo
            echo "valid targets (from plugins):"
            plugin__ls | sed 's/^/    /'
        } >&2
        es=2
    fi
    return "$es"
}

target__run() {
    #
    # Run target $1
    #
    local target=$1
    local module
    local fn
    read -r module fn <<<"$(
        target__map \
          | tr : ' ' \
          | grep -we "^$target" \
          | cut -d' ' -f2-
    )"
    debug_var target module fn
    mkit_import "$module"
    "$fn"
}
