#!/bin/bash
#shellcheck disable=SC2034
# mkit - simple install helper
# See LICENSE file for copyright and license details.

usage() {
    print_help >&2
    test $# -gt 0 && {
        printf >&2 '%s\n' "" "$@"
    }
    exit 2
}

print_help() {
    local self=${0##*/}
    local lines=(
        "usage:"
        "  $self TARGET"
        "  $self -i|--ini-1value PATH"
        "  $self -I|--ini-values PATH"
        "  $self --list-plugins"
        "  $self --list-targets"
        "  $self -V|--version-semver"
        "  $self --version"
        ""
        "commands:"
        "   TARGET          build target TARGET"
        "   -i PATH         print single-line value at INI path PATH"
        "                   (ignore all but last instance of this"
        "                   value in the INI)"
        "   -I PATH         like -i, but list all instances, one per line"
        "                   (multi-line values)"
        "   --list-plugins  print list of found plugins and exit"
        "   --list-targets  print list of valid targets and exit"
        "   -V|--version    print version (in SemVer format) and exit"
        "   --help          print this help text and exit"
        ""
    )
    printf '%s\n' "${lines[@]}"
    echo "valid targets (built-in):"
    target__ls | sed 's/^/    /'
    echo
    echo "valid targets (from plugins):"
    plugin__ls | sed 's/^/    /'
}


init_core() {
    #
    # Load core module (or die)
    #
    #shellcheck disable=SC1090,SC1091
    . "$MKIT_DIR/include/mkit.sh" \
     && . "$MKIT_DIR/include/vars.sh" \
     && return 0
    echo "failed to load core; check if MKIT_DIR is set properly: $MKIT_DIR" >&2
    exit 9
}

#
# Path to MKit dir (where 'include' is)
#
MKIT_DIR=${MKIT_DIR:-$(dirname "$0")}

just_ini() {
    #
    # Just do one mkit.ini operation $1
    #
    local op=$1
    local key=$2
    mkit_init || return $?
    ini "$op" "$key"
}

main () {
    init_core
    case "$1" in
        -V|--version)           echo "$MKIT_VERSION"; exit 0 ;;
        -i|--ini-1value)        just_ini 1value "$2"; exit $? ;;
        -I|--ini-values)        just_ini values "$2"; exit $? ;;
        --ini-lskeys)           just_ini lskeys "$2"; exit $? ;;
        --ini-lssect)           just_ini lssect "$2"; exit $? ;;
        --ini-sec)              just_ini sec    "$2"; exit $? ;;
        --list-targets)         mkit_init; target__ls; plugin__ls; exit 0 ;;
        --list-plugins)         mkit_init; plugin__ls; exit 0 ;;
        --help)                 print_help; exit 0 ;;
        -*)                     usage "unknown argument: $1" ;;
    esac
    test "$#" -gt 0 || usage
    mkit_init
    target__route "$@"
}

main "$@"
